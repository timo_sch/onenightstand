# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table feedback (
  id                            bigint auto_increment not null,
  question_one                  varchar(255),
  question_two                  varchar(255),
  question_three                varchar(255),
  question_four                 varchar(255),
  question_five                 varchar(255),
  question_three_selection      integer,
  constraint pk_feedback primary key (id)
);

create table student (
  id                            bigint auto_increment not null,
  nick_name                     varchar(255),
  password                      varchar(255),
  email                         varchar(255),
  points                        tinyint,
  semester                      tinyint,
  constraint pk_student primary key (id)
);


# --- !Downs

drop table if exists feedback;

drop table if exists student;

