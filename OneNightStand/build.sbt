name := "OneNightStand"

version := "1.0"

lazy val `onenightstand` = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"


libraryDependencies ++= Seq( jdbc , cache , ws   , specs2 % Test)

libraryDependencies += javaJdbc

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)