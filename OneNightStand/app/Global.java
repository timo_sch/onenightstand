import models.FDStat;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.api.mvc.Handler;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.lang.reflect.Method;
import java.util.concurrent.CompletionStage;

/**
 * Created by alex on 02.06.16.
 */
public class Global extends GlobalSettings {
    @Override
    public void beforeStart(Application app) {
        super.beforeStart(app);
    }

    @Override
    public void onStart(Application app) {
        super.onStart(app);
    }

    @Override
    public void onStop(Application app) {
        super.onStop(app);
    }

    @Override
    public CompletionStage<Result> onError(Http.RequestHeader request, Throwable t) {
        return super.onError(request, t);
    }

    @Override
    public Action onRequest(Http.Request request, Method actionMethod) {
        return super.onRequest(request, actionMethod);
    }

    @Override
    public Handler onRouteRequest(Http.RequestHeader request) {
        return super.onRouteRequest(request);
    }

    @Override
    public CompletionStage<Result> onHandlerNotFound(Http.RequestHeader request) {
        return super.onHandlerNotFound(request);
    }

    @Override
    public CompletionStage<Result> onBadRequest(Http.RequestHeader request, String error) {
        return super.onBadRequest(request, error);
    }
}
