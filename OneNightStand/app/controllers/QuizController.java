package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.quiz;

/**
 * Created by timo on 02.06.16.
 */
public class QuizController extends Controller {

    public Result quiz() {
        return ok(quiz.render());
    }
}
