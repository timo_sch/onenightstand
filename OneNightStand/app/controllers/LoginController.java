package controllers;

import models.FDStat;
import models.Feedback;
import models.Student;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.dashboard;
import views.html.feedback;
import views.html.login;
import views.html.monitoring;

import java.util.ArrayList;

/**
 * Created by alex on 01.06.16.
 */
public class LoginController extends Controller{

    public Result index() {
        return ok(login.render());
    }

    public Result login() {
        FDStat.incomingLogin++;

        return ok(login.render());
    }

    public Result signup() {
        DynamicForm form = Form.form().bindFromRequest(request());
        Student student = new Student();
        student.NickName = form.data().get("first_name");
        student.Semester = Byte.valueOf(form.data().get("last_name"));
        student.Password = form.data().get("password");
        student.Email = form.data().get("email");
        student.save();
        ctx().session().put("email",student.Email);
        FDStat.IncomingSignUp ++;
        return redirect("/dashboard");
    }
    public Result dashboard() {
        String email = ctx().session().get("email");
        ArrayList<Student> list = new ArrayList<>(Student.find.all());
        Student s = Student.find.where().eq("email", ctx().session().get("email")).findUnique();
        if (s != null){
            return ok(dashboard.render(s));
        }else  {
            return ok(login.render());
        }
    }

    public Result feedback() {
        String email = ctx().session().get("email");
        ArrayList<Student> list = new ArrayList<>(Student.find.all());
        Student s = Student.find.where().eq("email", ctx().session().get("email")).findUnique();
        if (s != null){
            return ok(feedback.render(s));
        }else  {
            return ok(login.render());
        }
    }

    public Result thanks() {
        String email = ctx().session().get("email");
        ArrayList<Student> list = new ArrayList<>(Student.find.all());
        Student s = Student.find.where().eq("email", ctx().session().get("email")).findUnique();
        if (s != null){
            return ok(views.html.thanks.render(s));
        }else  {
            return ok(login.render());
        }
    }

    public Result feedResult() {
        DynamicForm form = Form.form().bindFromRequest(request());
        Feedback feedback = new Feedback();
        feedback.questionOne   = form.get("question1");
        feedback.questionTwo   = form.get("question2");
        feedback.questionThree = form.get("question3");
        feedback.questionFour  = form.get("question4");
        feedback.questionFive  = form.get("question5");
        feedback.questionThreeSelection = Integer.valueOf(form.get("question3sel"));
        feedback.save();
        return ok(views.html.thanks.render(Student.find.where().eq("email",ctx().session().get("email")).findUnique()));
    }


    public Result monitoring() {

        return ok(monitoring.render(FDStat.incomingLogin, FDStat.IncomingSignUp));
    }


    private void sendEmail() {

    }
}