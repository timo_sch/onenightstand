package controllers;


import models.Student;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.election;
import views.html.login;

import static play.mvc.Results.ok;

/**
 * Created by timo on 02.06.16.
 */
public class ElectionController extends Controller {

    public Result election() {
        Student s = Student.find.where().eq("email",ctx().session().get("email")).findUnique();
        return ok(election.render(s));
    }
}
