package models;


import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by alex on 01.06.16.
 */

@Entity
public class Student extends Model{

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    public long ID;

    public String NickName;
    public String Password;
    public String Email;

    public byte Points;
    public byte Semester;

    public static Finder<Long, Student> find = new Finder<Long,Student>(Student.class);

    //   public int Rank;
 //   public int RankName;
}
