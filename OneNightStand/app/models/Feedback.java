package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by alex on 02.06.16.
 */
@Entity
public class Feedback extends Model {

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    public long ID;

    public String questionOne;
    public String questionTwo;
    public String questionThree;
    public String questionFour;
    public String questionFive;

    public int questionThreeSelection;

    public static Finder<Long, Student> find = new Finder<Long,Student>(Student.class);

}
